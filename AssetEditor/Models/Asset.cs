﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssetEditor.Models
{
    public class Asset
    {
        public enum Currencys { RUB, USD, EUR, GBP, KZT }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public int Money { get; set; }
        public Currencys Currency { get; set; }

        public int StartPrice { get; set; }
        public int ResidualPrice { get; set; }
        public int EstimatedPrice { get; set; }
       
    }
}