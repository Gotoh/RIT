﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssetEditor.Models;

namespace AssetEditor.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        
        public ActionResult Index()
        {
            ViewBag.Assets = StaticData.AssetsList; //список всех активов

            return View();
        }

        /// <summary>
        /// Удаление актива
        /// </summary>
        /// <param name="assetId">Id актива</param>
        /// <returns></returns>
        [HttpPost]
        public string DeleteAsset(int assetId)
        {
            try
            {
                StaticData.AssetsList.Remove(StaticData.AssetsList.FirstOrDefault(x => x.Id == assetId));
                return "0";
            }
            catch (Exception er)
            {
                return er.Message;
            }
           
        }


        /// <summary>
        /// Форма добавления актива
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddAsset()
        {
            return PartialView("_AddAsset");
        }

        /// <summary>
        /// Сохранение нового актива
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public string AddAsset(Asset asset, bool type=false)
        {
            try
            {
                if (!type)
                {
                    asset.Id = StaticData.AssetsList.Last().Id + 1;
                    asset.EstimatedPrice = 0;
                    asset.ResidualPrice = 0;
                    asset.StartPrice = 0;
                    StaticData.AssetsList.Add(asset);
                    return " <tr class=\"trClick\" id=" + asset.Id + "><td> " + asset.Name + "</td><td>" +
                           asset.Discription + "</td><td colspan=\"3\">" + asset.Money + "</td><td>" + asset.Currency + "</td></tr>";
                }
                else
                {
                    asset.Id = StaticData.AssetsList.Last().Id + 1;
                    asset.Money = 0;
                    StaticData.AssetsList.Add(asset);
                    return " <tr class=\"trClick\" id=" + asset.Id + "><td> " + asset.Name + "</td><td>" +
                          asset.Discription + "</td><td>Нач. " + asset.StartPrice + "</td><td> Оцен." + asset.EstimatedPrice + "</td><td>ост." + asset.ResidualPrice + "</td><td>" + asset.Currency + "</td></tr>";
                }
               
            }
            catch (Exception)
            {
                return "-1";
            }
           
        }

        /// <summary>
        /// Форма редактирования актива
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EditAsset(int assetId)
        {
            ViewBag.Asset = StaticData.AssetsList.FirstOrDefault(x => x.Id == assetId);
            return PartialView("_EditAsset");
        }

        /// <summary>
        /// Сохранение измененного актива
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpPost]
        public string EditAsset(Asset asset, bool type = false)
        {
            try
            {
                if (!type)
                {
                    asset.EstimatedPrice = 0;
                    asset.ResidualPrice = 0;
                    asset.StartPrice = 0;
                    StaticData.AssetsList[StaticData.AssetsList.FindIndex(x => x.Id == asset.Id)] = asset;
                    return "<td> " + asset.Name + "</td><td>" +
                           asset.Discription + "</td><td colspan=\"3\">" + asset.Money + "</td><td>" + asset.Currency + "</td>";
                }
                else
                {
                    asset.Money = 0;
                    StaticData.AssetsList[StaticData.AssetsList.FindIndex(x => x.Id == asset.Id)] = asset;
                    return "<td> " + asset.Name + "</td><td>" +
                          asset.Discription + "</td><td>" + asset.StartPrice + "</td><td> Оцен." + asset.EstimatedPrice + "</td><td>ост." + asset.ResidualPrice + "</td><td>" + asset.Currency + "</td>";
                }

            }
            catch (Exception er)
            {
                return "-1";
            }
        }


    }
}