﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssetEditor.Models;

namespace AssetEditor
{
    public static class StaticData
    {
        public static List<Asset> AssetsList;

        public static void InitAssetsList()
        {
            AssetsList = new List<Asset>();
            AssetsList.Add(new Asset() { Discription = "на счету № 5 в ЕвроВорБанке", Name = "ЕвроВорБанк", Money = 1000, Currency = Asset.Currencys.RUB, Id = 1 });
            AssetsList.Add(new Asset() { Discription = "на счету № 3 во Внешторгабке", Name = "Внешторгабка ", Money = 5, Currency = Asset.Currencys.USD, Id = 2 });
            AssetsList.Add(new Asset() { Discription = "в кассе", Name = "Касса", Money = 100, Currency = Asset.Currencys.RUB, Id = 3 });
            AssetsList.Add(new Asset() { Discription = "в кассе талонов на бензин от Аспека", Name = "Аспэк", Money = 3000, Currency = Asset.Currencys.RUB, Id = 4 });
            AssetsList.Add(new Asset() { Discription = " по адресу Бассейная-6, год постройки 1970", Name = "торговое здание", StartPrice = 3000, EstimatedPrice = 1000000, ResidualPrice = 5000, Currency = Asset.Currencys.RUB, Id = 5 });
            AssetsList.Add(new Asset() { Discription = " 100 килограммов гвоздей 2000 года изготовления", Name = "Гвозди", StartPrice = 1000, EstimatedPrice = 2000, ResidualPrice = 100, Currency = Asset.Currencys.RUB, Id = 6 });

        }
    }
}