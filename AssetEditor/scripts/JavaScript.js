﻿var selectedTr; //выбранный актив
$(function () {
    //Выбор актива в списке
    $('.AssetsList').on('click', '.trClick', function () {
        $("#" + selectedTr).removeClass("SelectedTr");
        selectedTr = this.id;
        $(this).addClass("SelectedTr");
        $(".DisabledButton").removeClass("DisabledButton").prop("disabled", false);
    });

    //Обработка меню
    $('#menu').on('click', 'button', function () {
        switch (this.id) {
        case "deleteButton":
            {
                if (confirm("Вы уверены?")) {
                    $.ajax({
                        type: 'POST',
                        url: '/Home/DeleteAsset',
                        data: { assetId: selectedTr },
                        success: function(data) {
                            if (data === "0") {
                                $('#' + selectedTr).remove();
                                $("#deleteButton, #editButton").addClass("DisabledButton").prop("disabled", true);;
                            } else {
                                alert(data);
                            }
                        }

                    });
                } else {
                    return;
                }
            }
            break;
        case "addButton":
        {
            $.ajax({
                type: 'GET',
                url: '/Home/AddAsset',
                success: function (data) {
                    $('.PopupEditAsset').empty().append(data).show();
                    $('.Overlay').show();
                }

            });
        }
            case "editButton":
                {
                    $.ajax({
                        type: 'GET',
                        url: '/Home/EditAsset',
                        data: { assetId: selectedTr },
                        success: function (data) {
                            $('.PopupEditAsset').empty().append(data).show();
                            $('.Overlay').show();
                           $("#notMoney").trigger("click");
                        }

                    });
                }
            break;
        default:
            return; 
        }
    });
    //скрыть попуп по клику на оверлей
    $('body').on('click', '.Overlay', function () {
        $(this).hide();
        $('.PopupEditAsset').empty().hide();;
     
    });

    //отправить новый актив на сервер
    $('.PopupEditAsset').on('click', '#saveNewAsset', function () {
        if (/^[ 0-9]+$$/.test($('[name="Money"]').val()) && $('[name="Name"]').val() !== "") {
            $.ajax({
                type: 'POST',
                url: '/Home/AddAsset',
                data: $('#addAssetForm').serialize(),
                success: function (data) {
                    if (data !== "-1") {
                        $('.PopupEditAsset').empty().hide();
                        $('.Overlay').hide();
                        $('.AssetsTable').append(data);
                    } else {
                        alert("Ошибка при сохранении");
                    }

                }

            });
        }
     
    });

    //отправить измененный актив на сервер
    $('.PopupEditAsset').on('click', '#editAsset', function () {
        if (/^[ 0-9]+$$/.test($('[name="Money"]').val()) && $('[name="Name"]').val() !== "") {
            $.ajax({
                type: 'POST',
                url: '/Home/EditAsset',
                data: $('#addAssetForm').serialize()+"&Id="+selectedTr,
                success: function (data) {
                    if (data !== "-1") {
                        $('.PopupEditAsset').empty().hide();
                        $('.Overlay').hide();
                        $('#' + selectedTr).empty().html(data).removeClass("SelectedTr");
                        $('#editButton, #deleteButton').addClass("DisabledButton").prop("disabled", true);
                       } else {
                        alert("Ошибка при сохранении");
                    }

                }

            });
        }

    });
    //смена типа актива 
    $('body').on('click', '#notMoney', function () {
        if ($(this).prop("checked")) {
            $('.notMoneyText').show();
            $('.MoneyText').prop("hidden", true);
            var m = $('[name="Money"]');
            if (!/^[ 0-9]+$$/.test($('[name="Money"]').val())) {
                $('[name="Money"]').val(0);
            }
           
        } else {
            $('.notMoneyText').hide();
            $('.MoneyText').prop("hidden", false);
        }
       
    });

});